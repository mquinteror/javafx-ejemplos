// Ejemplo de javaFX Label

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;

public class JavaFXLabelDemo extends Application {
    public static void main ( String[] args ){
	launch( args );
    }

    // sobreescribir el metodo start
    public void start ( Stage myStage ){

	// dar un titulo al ecenario
	myStage.setTitle("Demostracion de javaFx label");

	// usando un flowPane para el nodo principal
	FlowPane rootNode = new FlowPane();

	// crear la ecena
	Scene myScene = new Scene ( rootNode, 900, 600 );

	// fijar la ecena en el acenario
	myStage.setScene( myScene );

	// crear una etiqueta
	Label etiqueta = new Label( "Esta es una etiqueda de JavaFX" );

	// agregar la escena al flowpane ( root node )
	rootNode.getChildren().add( etiqueta );

	// mostrar el scenario en esta escena

	myStage.show();
    }
}
