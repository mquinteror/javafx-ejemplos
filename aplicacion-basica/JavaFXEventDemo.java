// dejemplo de demostracion de eventos y botones

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.event.*;
import javafx.geometry.*;

public class JavaFXEventDemo extends Application{
    Label etiqueta;
    public static void main ( String[] args ){
	launch( args );
    }
    // sobreescribiendo el metodo start
    public void start ( Stage escenario ){
	//darle un titulo al escenario
	escenario.setTitle(" ejempo de uso de eventos y botones ");
	FlowPane plantilla = new FlowPane(10,10);

	// centrar los controles en el centro
	plantilla.setAlignment(Pos.CENTER);

	// crear una escena
	Scene escena = new Scene( plantilla, 900,600 );

	// agregar la escena al escenario
	escenario.setScene( escena );

	// crear una etiqueta
	etiqueta = new Label("pulsa un boton");

	// crear dos botones apretables
	Button botonAlfa = new Button("Alfa");
	Button botonBeta = new Button("Beta");

	// manejar los eventos del boton alfa
	botonAlfa.setOnAction(new EventHandler<ActionEvent>(){
		public void handle( ActionEvent ae ){
		    etiqueta.setText("se ha presionado el boton alfa");
		}
	    });
	// manejas los eventos del boton beta
	botonBeta.setOnAction( new EventHandler<ActionEvent>(){
		public void handle ( ActionEvent ae ){
		    etiqueta.setText("se ha presionado del boton beta");
		}
	    });
	// agregar la etiqueta y los botones a la plantilla
	plantilla.getChildren().addAll(botonAlfa,botonBeta,etiqueta);

	// mostrar el escenario y su escena
	escenario.show();
    }
}

