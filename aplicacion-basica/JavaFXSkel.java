// a java fx application skeleton

import javafx.application.*;
import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.layout.*;

public class JavaFXSkel extends Application {
    public static void main ( String[] args ){
	System.out.println("iniciando la aplocacion de javafx");

	// se inicia la aplicacion javafx mediante el metodo launch
	launch( args );
    }

    // sobreescritura del metodo init
    public void init (){
	System.out.println("dentro del metodo init");
    }

    // dsobreescritura del metodo start
    public void start ( Stage myStage ){
	System.out.println("Estamos dentro del metodo start");

	// dar titulo al scenario
	myStage.setTitle("Esqueleto de una aplicacion de javafx");

	// crear un nodo principal, en este caso un flowpane
	FlowPane rootNode = new FlowPane();

	// crear una escena
	Scene myScene = new Scene ( rootNode, 900,600 );

	// establecer la ecena en el escenario
	myStage.setScene( myScene );

	// mostrar el scenario en esta scena
	myStage.show();
    }

    // sobreescribir el metodo stop
    public void stop (){
	System.out.println("estamos dentro del metodo stop()");
    }
}
